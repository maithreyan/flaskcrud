from flask import Flask, request, jsonify
import flask_sqlalchemy as sql
from flask_marshmallow import Marshmallow

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:adventurE@28@localhost/upe'
db = sql.SQLAlchemy(app)
ma = Marshmallow(app)


class Info(db.Model):
    __tablename__ = 'bioo'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=True)
    age = db.Column(db.Integer, unique=False)

    def __init__(self, name, age):
        self.name = name
        self.age = age


db.create_all()


class PostSchema(ma.Schema):
    class Meta:
        fields = ("name", "age")


post_schema = PostSchema()
posts_schema = PostSchema(many=True)


@app.route('/home', methods=['GET'])
def Home():
    return jsonify()


@app.route('/home/add', methods=['POST'])
def Add():
    name = request.json['name']
    age = request.json['age']

    my_data = Info(name, age)
    db.session.add(my_data)
    db.session.commit()
    return post_schema.jsonify(my_data)

    # my_data=F.jsonify(name,age)
    # a=Info(my_data)
    # db.session.add(a)
    # db.session.commit()


@app.route('/home/view', methods=['GET'])
def ViewAll():
    all_posts=Info.query.all()
    a=posts_schema.dump(all_posts)
    return jsonify(a)

@app.route('/home/view/<id>', methods=['GET'])
def ViewOne(id):
    record=Info.query.get(id)
    return post_schema.jsonify(record)

@app.route('/home/update/<id>', methods=['PUT'])
def Update(id):
    record=Info.query.get(id)
    name = request.json['name']
    age = request.json['age']

    record.name=name
    record.age=age

    db.session.commit()
    return post_schema.jsonify(record)

@app.route('/home/delete/<id>', methods=['DELETE'])
def Delete(id):
    record = Info.query.get(id)
    db.session.delete(record)
    db.session.commit()
    return post_schema.jsonify(record)

if __name__ == '__main__':
    app.run()


# pip freeze > requirements.txt   >>> used to generate atext file with all the porject packages used in this
